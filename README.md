# Information

This is a step by step tutorial of I set up my dell precision m4800.
Some parts are very specific for my notebook.
So when you follow it step by step. Think at every step if you really need it :)

# Prepare

1. Download manjaro gnome: https://manjaro.org/downloads/official/gnome/ (default version)
2. Check the signature with the command `sha1sum manjaro-gnome-20.2-201203-linux59.iso`
3. use `BalenaEtcher` to write it on your USB-Stick

# install gnome

1. boot the stick and select open sources drivers
2. launch installer and install gnome
3. reboot

# configure gnome

1. open layout manager
2. set layout to gnome
3. go to settings and deactivate manjaro branding

## Grub
- open the file `/etc/default/grub` and set `GRUB_GFXMODE=1920x1080`
- run `sudo update grub`

## tweaks

- open tweaks
- disable `suspend when laptop lid closed`
- remove Manjaro hello from startup applications

## zsh shell

- zsh should be already the default shell (if not https://wiki.archlinux.org/index.php/zsh)
- run: `autoload -Uz zsh-newuser-install`
- start zsh newuser install: `zsh-newuser-install -f`
- next run `sudo pacman -S zsh-autosuggestions` and `source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh`

# yay AUR helper

- `sudo pacman -S base-devel` (select all)
- `sudo pacman -S yay`

# grafic drivers

- search for manjaro settings
- go to hardware
- run auto install proprietary driver
- go back and go to kernels
- install a second kernel as fallback e.g. lts

# config
## disable supsend on lid switch at boot time
I want to have my notebook always with a closed lid, beacuse I just use my external screen.
I had the problem that when I came to the login screen the system sets into suspend mode.

to fix this I need to change the file: `/etc/systemd/logind.conf`
and set this 4 lines:
```
HandleLidSwitch=ignore
HandleLidSwitchExternalPower=ignore
HandleLidSwitchDocked=ignore
LidSwitchIgnoreInhibitied=no
```

# setup i3

- install: `sudo pacman -S gnome-flashback i3-wm i3-gaps feh flameshot i3status-rust muparser ttf-font-awesome lxterminal`
- `yay -S i3-gnome`
- `yay -S ulauncher`
- create folder `~/.config/i3` and add a new file `config`
    - copy this file: [i3_config](i3_config)
- create another folder `~/.config/i3status-rust` and add a new file `config.toml`
    - copy this file: [i3_status_rust](i3_status_rust.toml)
- create another folder `~/.startupScripts`
    - copy this file to get gnome background: [getBackground.sh](getBackground.sh)
    - run `chmod +x getBackground.sh` to make the script executable
- reboot on login screen select your user and on the bottom right select i3-gnome
- now you should start the i3 shell.

## disable gnome desktop window

- run in terminal `gsettings set org.gnome.gnome-flashback desktop false`

## albert
- open terminal with super+enter
- type `albert`
- set hotkey to super+d
- in extensions add applications, system and calculator
    - change restart to `reboot`
    - shutdown to `shutdown -h now`

# applications

- telegram: `sudo pacman -S telegram-desktop`
- spotify: `yay -S spotify`
- vs code: `yay -S visual-studio-code-bin`
- bitwarden: `yay -S bitwarden`
- voltajs (node version manager): `yay -S volta`
    - run `volta setup` after installing node and yarn do use it
- etcher (to flash usb sticks, sd cards, ...)
    - `sudo pacman -S etcher`
- clipit clipboardmanager
    - `yay -S clipit`
    - open settings
        - automatically paste selected item
        - hotkeys
            - set hotkey for history to `<Ctrl><Shift><Alt>V`
- vlc video player `sudo pacman -S vlc`

## disable beep in the whole system

- The PC speaker can be disabled by unloading the pcspkr kernel module: `sudo rmmod pcspkr`
- Blacklisting the pcspkr module will prevent udev from loading it at boot: `echo "blacklist pcspkr" | sudo tee /etc/modprobe.d/nobeep.conf`

## disable beep on find in Firefox

- open `about:config`
- search for `accessibility.typeaheadfind.enablesound`
    - disable it
