# Work notebook setup

# Information

This is a step by step tutorial of I set up my dell xps 13.
Some parts are very specific for my notebook.
So when you follow it step by step. Think at every step if you really need it :)

# Prepare

1. Download manjaro gnome: https://manjaro.org/downloads/official/gnome/ (default version)
2. Check the signature with the command `sha1sum manjaro-gnome-20.2-201203-linux59.iso`
3. use `BalenaEtcher` to write it on your USB-Stick

# install gnome

1. boot the stick and select open sources drivers
2. launch installer and install gnome
3. reboot

## tweaks

- open tweaks
- remove Manjaro hello from startup applications

## zsh shell

- zsh should be already the default shell (if not https://wiki.archlinux.org/index.php/zsh)
- run: `autoload -Uz zsh-newuser-install`
- start zsh newuser install: `zsh-newuser-install -f`
- next run `sudo pacman -S zsh-autosuggestions` and `source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh`

# yay AUR helper

- `sudo pacman -S base-devel` (select all)
- `sudo pacman -S yay`

# Install font

- `sudo pacman -S ttf-jetbrains-mono`

# setup i3

- install: `sudo pacman -S gnome-flashback i3-wm i3-gaps feh flameshot i3status-rust muparser ttf-font-awesome gnome-todo lxterminal`
- `yay -S i3-gnome`
- `yay -S ulauncher`
- create folder `~/.config/i3` and add a new file `config`
    - copy this file: [i3_config](i3_config)
- create another folder `~/.config/i3status-rust` and add a new file `config.toml`
    - copy this file: [i3_status_rust](i3_status_rust.toml)
- create another folder `~/.startupScripts`
    - copy this file to get gnome background: [getBackground.sh](../getBackground.sh)
    - run `chmod +x getBackground.sh` to make the script executable
    - TODO: copy the file [startToWork.sh](startToWork.sh), to have your work programms open automatically
    - TODO: run `chmod +x startToWork.sh` to make the script executable
- reboot on login screen select your user and on the bottom right select i3-gnome
- now you should start the i3 shell.

## disable gnome desktop window

- run in terminal `gsettings set org.gnome.gnome-flashback desktop false`

## ulauncher
- open terminal with super+enter
- type `ulauncher`
- set hotkey to super+d
- Add extensions
    - system: https://github.com/iboyperson/ulauncher-system

# applications

- telegram: `sudo pacman -S telegram-desktop`
- spotify: `yay -S spotify`
- vs code: `yay -S visual-studio-code-bin`
- vim: `sudo pacman -S vim`
- bitwarden: `yay -S bitwarden`
- voltajs (node version manager): `yay -S volta`
    - run `volta setup` after installing node and yarn do use it
- etcher (to flash usb sticks, sd cards, ...)
    - `sudo pacman -S etcher`
- clipit clipboardmanager
    - `yay -S clipit`
    - open settings
        - automatically paste selected item
        - hotkeys
            - set hotkey for history to `<Ctrl><Shift><Alt>V`
- vlc video player `sudo pacman -S vlc`

# Notifcations WIP

- disable gnome notifications: `gsettings set org.gnome.gnome-flashback notifications false`
- install xfce: `sudo pacman -S xfce4-notifyd`
- 