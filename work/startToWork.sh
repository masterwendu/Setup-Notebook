#!/bin/bash

# start programs from shell but immediately disown them
startAndDisown() {
    $1 & disown $! 
}

# start vscode on workspace 1
i3-msg 'workspace 1;'
startAndDisown code &
sleep 2

# start chrome on workspace 2
i3-msg 'workspace 2;'
startAndDisown google-chrome-stable &
sleep 3

i3-msg 'workspace 3;'
startAndDisown firefox &
sleep 5

i3-msg 'workspace 4;' &
startAndDisown 'google-chrome-stable --new-window https://wekan.woodlan.de/b/3qfWpzLfiCk599AEE/woom https://redmine.woombikes.com/projects' &
sleep 3

i3-msg 'workspace 2;' &

# start scratchpad apps
startAndDisown slack &
startAndDisown spotify &

exit 0